"""Main script for generating output.csv."""
import pandas as pd


def main():
    # Import raw CSV data
    raw_pitch = pd.read_csv('data/raw/pitchdata.csv')

    # Create Split Label Sets
    versus_h = ['vs LHH', 'vs RHH']
    versus_p = ['vs LHP', 'vs RHP']

    # Create set of DataFrames to be tabulated
    frames = {'HitterId': versus_p, 'HitterTeamId': versus_p,
              'PitcherId': versus_h, 'PitcherTeamId': versus_h}

    # Process set of DataFrames for each Split
    proc_dfs = []
    for i, j in frames.iteritems():
        min_l, min_r = get_lr_minset(raw_pitch, i)
        id_df = pd.concat([create_out_df(compute_df(min_l), j[0]),
                           create_out_df(compute_df(min_r), j[1])])
        proc_dfs.append(id_df)

    # Concatenate results and sort
    out_df = pd.concat(proc_dfs).sort_values(['SubjectId', 'Stat', 'Split',
                                              'Subject'], ascending=True)

    # Output to CSV
    out_df.to_csv('data/processed/output.csv', index=False)


# Function get_lr_minset
# Generate DataFrames for an ID column with pertinent stats for both L and R
# opposition Hitter/Pitcher sides. Each set has a minimum of 25 total PA.
# Input: in_df - DataFrame with raw pitch data
#        id_col - Id Column to group by
# Output: l_min - DataFrame with minimum PA vs LH
#         r_min - DataFrame with minimum PA vs RH
def get_lr_minset(in_df, id_col):
    if id_col[0:3] == 'Hit':
        opp_id = "PitcherSide"
    else:
        opp_id = "HitterSide"

    l_all = in_df[[id_col, 'PA', 'AB', 'H', '2B', '3B', 'HR', 'BB', 'SF',
                   'HBP']][in_df[opp_id] == 'L']
    r_all = in_df[[id_col, 'PA', 'AB', 'H', '2B', '3B', 'HR', 'BB', 'SF',
                   'HBP']][in_df[opp_id] == 'R']

    l_min = l_all.groupby(id_col).filter(lambda x: x.sum()['PA'] >= 25)\
                 .groupby(id_col).sum()
    r_min = r_all.groupby(id_col).filter(lambda x: x.sum()['PA'] >= 25)\
                 .groupby(id_col).sum()

    return l_min, r_min


# Function avg
# Compute Batting Average
# Input: x - DataFrame row
# Return: Computed Batting Average
def avg(x):
    return round(float(x['H']) / x['AB'], 3)


# Function obp
# Compute On Base Percentage
# Input: x - DataFrame row
# Return: Computed OBP
def obp(x):
    return round(float(x['H'] + x['BB'] + x['HBP']) / (x['AB'] + x['BB'] +
                 x['HBP'] + x['SF']), 3)


# Function slg
# Compute Slugging Percentage
# Input: x - DataFrame row
# Return: Computed SLG
def slg(x):
    return round(float(x['H'] + x['2B'] + x['3B']*2 + x['HR']*3) / x['AB'], 3)


# Function ops
# Compute On Base Plus Slugging
# Input: x - DataFrame row
# Return: Computed OBP
def ops(x):
    return round(float(x['H'] + x['BB'] + x['HBP']) / (x['AB'] + x['BB'] +
                 x['HBP'] + x['SF']) + float(x['H'] + x['2B'] + x['3B']*2 +
                 x['HR']*3) / x['AB'], 3)


# Function compute_df
# Apply Stat computations into a new DataFrame
# Input: in_df - DataFrame containing key stats summed and filtered with
#                PA minimum
# Output: out_df - DataFrame containing computed stats
def compute_df(in_df):
    out_df = pd.DataFrame(index=in_df.index)
    out_df['AVG'] = in_df.apply(avg, axis=1)
    out_df['OBP'] = in_df.apply(obp, axis=1)
    out_df['SLG'] = in_df.apply(slg, axis=1)
    out_df['OPS'] = in_df.apply(ops, axis=1)

    return out_df


# Function create_out_df
# Concatenate stat lines based on Id, Split, Subject
# Input: comp_df - DataFrame of computed stats
# Return: Concantenated DataFrame of Split stats
def create_out_df(comp_df, split):
    ind_stat_dfs = []
    for col in comp_df:
        ind_stat_dfs.append(comp_df[[col]])

    gen_dfs = []
    for i in ind_stat_dfs:
        sub_id = i.index.name
        sub_name = i.columns[0]
        i.reset_index(level=0, inplace=True)
        gen_sub = i.rename(index=str, columns={sub_id: 'SubjectId'})
        gen_sv = gen_sub.rename(index=str, columns={sub_name: 'Value'})
        gen_sv.insert(1, 'Stat', sub_name)
        gen_sv.insert(2, 'Split', split)
        gen_sv.insert(3, 'Subject', sub_id)

        gen_dfs.append(gen_sv)

    return pd.concat(gen_dfs)

if __name__ == '__main__':
    main()
